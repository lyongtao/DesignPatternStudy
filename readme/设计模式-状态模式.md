@[TOC](目录)
# 简介
在状态模式（State Pattern）中，类的行为是基于它的状态改变的。这种类型的设计模式属于行为型模式。

在状态模式中，我们创建表示各种状态的对象和一个行为随着状态对象改变而改变的 context 对象。
# 实际应用场景
1、打篮球的时候运动员可以有正常状态、不正常状态和超常状态。

2、曾侯乙编钟中，'钟是抽象接口','钟A'等是具体状态，'曾侯乙编钟'是具体环境（Context）。
# UML
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210318094702452.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2x5b25nMTIyMw==,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210318094729803.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2x5b25nMTIyMw==,size_16,color_FFFFFF,t_70)

# 代码实现
```java
public class Context {

    private State state;

    public Context() {
        this.state = null;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}

```

```java
public interface State {

    void doAction(Context context);
}
```
```java
public class StartState implements State {
    @Override
    public void doAction(Context context) {
        System.out.println("start state...");
        context.setState(this);
    }

    @Override
    public String toString() {
        return "StartState.....";
    }
}
```

```java
public class EndState implements State {
    @Override
    public void doAction(Context context) {
        System.out.println("end state...");
        context.setState(this);
    }

    @Override
    public String toString() {
        return "EndState.....";
    }
}
```

```java
public class Main {

    public static void main(String[] args) {
        Context context = new Context();

        StartState startState = new StartState();
        startState.doAction(context);
        System.out.println(context.getState());

        EndState endState = new EndState();
        endState.doAction(context);
        System.out.println(context.getState());
    }
}
```
# 测试
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210318095124415.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2x5b25nMTIyMw==,size_16,color_FFFFFF,t_70)
