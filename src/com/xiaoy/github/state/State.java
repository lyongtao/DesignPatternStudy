package com.xiaoy.github.state;

/**
 * @author liuyongtao
 * @since 2021-3-18 8:29
 */
public interface State {

    void doAction(Context context);
}
