package com.xiaoy.github.state;

public class EndState implements State {
    @Override
    public void doAction(Context context) {
        System.out.println("end state...");
        context.setState(this);
    }

    @Override
    public String toString() {
        return "EndState.....";
    }
}
