package com.xiaoy.github.chain;

public class Main {

    public static AbsLogger getChainOfLoggers(){
        DebugLogger debugLogger = new DebugLogger(AbsLogger.debugger);
        InfoLogger infoLogger = new InfoLogger(AbsLogger.info);
        ErrorLogger errorLogger = new ErrorLogger(AbsLogger.error);

        debugLogger.setNextLogger(infoLogger);
        infoLogger.setNextLogger(errorLogger);
        return debugLogger;
    }

    public static void main(String[] args) {
        AbsLogger chain = getChainOfLoggers();
//        chain.logMsg(AbsLogger.debugger,"我是 debugger...");
//        chain.logMsg(AbsLogger.info,"我是 info...");
        chain.logMsg(AbsLogger.error,"我是 error...");
    }
}
