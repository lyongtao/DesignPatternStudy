package com.xiaoy.github.chain;

public class ErrorLogger extends AbsLogger {

    public ErrorLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String msg) {
        System.out.println("ErrorLogger::logger " + msg);
    }
}
