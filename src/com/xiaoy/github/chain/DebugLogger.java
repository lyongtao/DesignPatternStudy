package com.xiaoy.github.chain;

public class DebugLogger extends AbsLogger {

    public DebugLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String msg) {
        System.out.println("DebugLogger::logger " + msg);
    }
}
