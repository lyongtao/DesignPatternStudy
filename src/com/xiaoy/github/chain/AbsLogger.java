package com.xiaoy.github.chain;

public abstract class AbsLogger {

    public static int debugger = 1;
    public static int info = 2;
    public static int error = 3;

    protected int level;
    private AbsLogger absLogger;

    public void setNextLogger(AbsLogger absLogger) {
        this.absLogger = absLogger;
    }

    public void logMsg(int level, String msg) {
        if (this.level <= level) {
            this.write(msg);
        }
        if (absLogger != null) {
            absLogger.logMsg(level, msg);
        }
    }

    protected abstract void write(String msg);

}
