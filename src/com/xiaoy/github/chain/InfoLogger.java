package com.xiaoy.github.chain;

public class InfoLogger extends AbsLogger {

    public InfoLogger(int level) {
        this.level = level;
    }

    @Override
    protected void write(String msg) {
        System.out.println("InfoLogger::logger " + msg);
    }
}
