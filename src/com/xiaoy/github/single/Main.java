package com.xiaoy.github.single;

/**
 * 测试
 *
 * @author xiaoy
 * @since 2021-3-22 9:49
 */
public class Main {
    public static void main(String[] args) {
        System.out.println(Single1.getInstance());
        System.out.println(Single1.getInstance());
        System.out.println("***********");
        System.out.println(Single2.getInstance());
        System.out.println(Single2.getInstance());
        System.out.println("***********");
        System.out.println(Single21.getInstance());
        System.out.println(Single21.getInstance());
        System.out.println("***********");
        System.out.println(Single3.getInstance());
        System.out.println(Single3.getInstance());
        System.out.println("***********");
        System.out.println(Single4.INSTANCE);
        System.out.println(Single4.INSTANCE);
        System.out.println("***********");
        System.out.println(Single5.getInstance());
        System.out.println(Single5.getInstance());

    }
}
