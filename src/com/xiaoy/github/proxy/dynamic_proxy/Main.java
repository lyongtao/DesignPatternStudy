package com.xiaoy.github.proxy.dynamic_proxy;

import com.xiaoy.github.proxy.Image;
import com.xiaoy.github.proxy.PathImage;
import com.xiaoy.github.proxy.RealImage;

/**
 * @author liuyongtao
 * @since 2021-3-15 14:11
 */
public class Main {

    public static void main(String[] args) {
        DynamicProxyHandler<Image> proxyHandler = new DynamicProxyHandler<>(new RealImage());
        proxyHandler.proxy().display();
        System.out.println("==========");
        proxyHandler = new DynamicProxyHandler<>(new PathImage());
        proxyHandler.proxy().display();
    }
}
