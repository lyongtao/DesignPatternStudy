package com.xiaoy.github.proxy;

/**
 * @author liuyongtao
 * @since 2021-3-15 12:10
 */
public class PathImage implements Image {
    @Override
    public void display() {
        System.out.println(PathImage.class.getName());
    }
}
