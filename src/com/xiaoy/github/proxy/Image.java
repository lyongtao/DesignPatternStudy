package com.xiaoy.github.proxy;

/**
 * @author liuyongtao
 * @since 2021-3-15 12:00
 */
public interface Image {

    void display();
}
