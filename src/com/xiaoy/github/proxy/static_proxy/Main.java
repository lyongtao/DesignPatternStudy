package com.xiaoy.github.proxy.static_proxy;

import com.xiaoy.github.proxy.PathImage;
import com.xiaoy.github.proxy.RealImage;

/**
 * @author liuyongtao
 * @since 2021-3-15 12:06
 */
public class Main {

    public static void main(String[] args) {
        ProxyImage proxyImage = new ProxyImage(new RealImage());
        proxyImage.display();
        System.out.println("==========");
        proxyImage = new ProxyImage(new PathImage());
        proxyImage.display();
    }
}
