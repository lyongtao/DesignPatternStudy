package com.xiaoy.github.proxy.static_proxy;

import com.xiaoy.github.proxy.Image;

/**
 * @author liuyongtao
 * @since 2021-3-15 12:02
 */
public class ProxyImage implements Image {

    private Image image;

    public ProxyImage(Image image) {
        this.image = image;
    }

    @Override
    public void display() {
        System.out.println(image.getClass().getName() + " 执行之前。。。");
        image.display();
        System.out.println(image.getClass().getName() + " 执行之后。。。");
    }

}
