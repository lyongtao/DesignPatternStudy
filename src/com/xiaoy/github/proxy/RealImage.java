package com.xiaoy.github.proxy;

/**
 * @author liuyongtao
 * @since 2021-3-15 12:01
 */
public class RealImage implements Image {
    @Override
    public void display() {
        System.out.println(RealImage.class.getName());
    }
}
