package com.xiaoy.github.factory;

public interface Shape {

    void draw();
}
