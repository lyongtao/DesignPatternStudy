package com.xiaoy.github.factory;

public class Main {

    public static void main(String[] args) {

        Shape shepe;
        ShapeFactory factory = new ShapeFactory();

        shepe = factory.getShepe(Square.class.getSimpleName());
        shepe.draw();

        shepe = factory.getShepe(Rectangle.class.getSimpleName());
        shepe.draw();

        shepe = factory.getShepe(Circle.class.getSimpleName());
        shepe.draw();

    }

}
