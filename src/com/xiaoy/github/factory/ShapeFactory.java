package com.xiaoy.github.factory;

public class ShapeFactory {

    public Shape getShepe(String shapeType) {

        Shape shape = null;

        if (Square.class.getSimpleName().equalsIgnoreCase(shapeType)) {
            shape = new Square();
        } else if (Circle.class.getSimpleName().equalsIgnoreCase(shapeType)) {
            shape = new Circle();
        } else if (Rectangle.class.getSimpleName().equalsIgnoreCase(shapeType)) {
            shape = new Rectangle();
        } else {
            throw new RuntimeException("没有找到对应的类型");
        }
        return shape;
    }

}
