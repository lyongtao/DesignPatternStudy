package com.xiaoy.demo;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author xiaoy
 * @since 2021-3-23 10:19
 */
public class SaveModeProxy implements InvocationHandler {

    private SaveMode saveMode;

    public SaveModeProxy(SaveMode saveMode) {
        this.saveMode = saveMode;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("代理模式 --> 归档前的逻辑...");
        Object invoke = method.invoke(saveMode, args);
        System.out.println("代理模式 --> 归档后的逻辑...");
        return invoke;
    }

    public SaveMode proxy() {
        return (SaveMode) Proxy.newProxyInstance(saveMode.getClass().getClassLoader(), saveMode.getClass().getInterfaces(), this);
    }
}
