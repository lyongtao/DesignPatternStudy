package com.xiaoy.demo;

/**
 * 1.获取不同的连接
 * 2.解析不同文件
 * 3.校验
 * 4.保存到不同的位置
 *
 * @author xiaoy
 * @since 2021-3-23 9:58
 */
public abstract class ProcessTemplate {

    abstract void getConnection();

    abstract void analysisFile();

    abstract void checkData();

    final void saveFile(String mode) {
        SaveModeFactory.saveMode(mode);
    }


    final void runProcess(String mode) {
        this.getConnection();
        this.analysisFile();
        this.checkData();
        this.saveFile(mode);
    }
}
