package com.xiaoy.demo;

/**
 * @author xiaoy
 * @since 2021-3-23 10:15
 */
public class DiskSave implements SaveMode {
    @Override
    public void save() {
        System.out.println("保存到 Disk...");
    }
}
