package com.xiaoy.demo;

/**
 * @author xiaoy
 * @since 2021-3-23 10:14
 */
public interface SaveMode {

    void save();
}
