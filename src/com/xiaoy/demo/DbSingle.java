package com.xiaoy.demo;

/**
 * 获取db 的连接（单例）
 *
 * @author xiaoy
 * @since 2021-3-23 10:09
 */
public enum DbSingle {

    INSTANCE;

    public void get() {
        System.out.println("单例模式 --> 获取 db 实例...");
    }

}
