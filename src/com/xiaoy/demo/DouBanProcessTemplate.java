package com.xiaoy.demo;

/**
 * 豆瓣数据同步模板
 *
 * @author xiaoy
 * @since 2021-3-23 10:31
 */
public class DouBanProcessTemplate extends ProcessTemplate {

    @Override
    void getConnection() {
        ConnectionFactory.connection("DouBan");
    }

    @Override
    void analysisFile() {
        System.out.println("模板方法 --> DouBan --> 数据解析...");
    }

    @Override
    void checkData() {
        System.out.println("模板方法 --> DouBan --> 数据校验...");
    }
}
