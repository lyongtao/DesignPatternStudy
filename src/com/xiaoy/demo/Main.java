package com.xiaoy.demo;

/**
 * 1.获取不同的连接
 * 2.解析不同文件
 * 3.不同的校验
 * 4.保存到不同的位置
 *
 * @author xiaoy
 * @since 2021-3-23 10:30
 */
public class Main {

    public static void main(String[] args) {
        ProcessTemplate template = new BaiDuProcessTemplate();
        template.runProcess("Yun");
        System.out.println("************");
        template.runProcess("Disk");

        System.out.println("************");

        template = new DouBanProcessTemplate();
        template.runProcess("Yun");
        System.out.println("************");
        template.runProcess("Disk");

    }
}
