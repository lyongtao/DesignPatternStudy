package com.xiaoy.demo;

/**
 * @author xiaoy
 * @since 2021-3-23 10:05
 */
public class SaveModeFactory {

    public static void saveMode(String saveMode) {

        SaveModeProxy saveModeProxy;

        if ("Yun".equalsIgnoreCase(saveMode)) {
            saveModeProxy = new SaveModeProxy(new YunSave());
        } else {
            saveModeProxy = new SaveModeProxy(new DiskSave());
        }
        saveModeProxy.proxy().save();
    }
}
