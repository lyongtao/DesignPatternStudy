package com.xiaoy.demo;

/**
 * 百度数据同步模板
 *
 * @author xiaoy
 * @since 2021-3-23 10:31
 */
public class BaiDuProcessTemplate extends ProcessTemplate {

    @Override
    void getConnection() {
        ConnectionFactory.connection("BaiDu");
    }

    @Override
    void analysisFile() {
        System.out.println("模板方法 --> BaiDu --> 数据解析");
    }

    @Override
    void checkData() {
        System.out.println("模板方法 --> BaiDu --> 数据校验");
    }
}
