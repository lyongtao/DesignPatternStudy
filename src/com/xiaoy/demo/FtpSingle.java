package com.xiaoy.demo;

/**
 * 单例
 *
 * @author xiaoy
 * @since 2021-3-23 10:09
 */
public enum FtpSingle {

    INSTANCE;

    public void get() {
        System.out.println("单例模式 --> 获取 ftp 实例...");
    }

}
