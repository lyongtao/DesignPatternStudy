package com.xiaoy.demo;

/**
 * 获取连接工厂
 *
 * @author xiaoy
 * @since 2021-3-23 10:05
 */
public class ConnectionFactory {

    public static void connection(String conType) {
        if ("ftp".equalsIgnoreCase(conType)) {
            FtpSingle.INSTANCE.get();
            System.out.println("工厂模式 --> 获取 ftp 连接...");
        } else {
            DbSingle.INSTANCE.get();
            System.out.println("工厂模式 --> 获取 db 连接...");
        }
    }
}
